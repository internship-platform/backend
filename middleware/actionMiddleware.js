module.exports = function authorizationMiddleware(req, res, next) {
  const requiredSecret = process.env.HASURA_GRAPHQL_ACTION_SECRET;
  const providedSecret = req.headers["ACTION_SECRET"];

  if (requiredSecret == providedSecret) next();
  else
    res.status(403).json({
      invalidActionSecret: "Action Secret is not valid",
    });
};
