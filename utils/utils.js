const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const nodemailer = require("nodemailer");

class utils {
  passwordIdDate = async (unhashedPassword) => {
    const password = await bcrypt.hash(unhashedPassword, 12);

    const id = crypto.randomBytes(3).toString("hex").toUpperCase();

    return {
      password,
      id,
    };
  };

  requireJWT = (payload, secretKey) => {
    return jwt.sign(payload, secretKey, { algorithm: "HS256" });
  };

  sendEmail = async (income, status, verify) => {
    let transporter = nodemailer.createTransport({
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      auth: {
        user: process.env.SMTP_USERNAME,
        pass: process.env.SMTP_PASSWORD,
      },
    });

    if (status) {
      return await transporter.sendMail({
        from: status.from,
        to: status.to,
        subject: status.subject,
        text: status.message,
        html: `<h1>Application Status</h1>
            <div>
            <p>${status.message}</p>
            <p>Your Application Status for the ${status.post} Internship Post has changed recently.</p>
            </div>`,
      });
    } else if (income) {
      return await transporter.sendMail({
        from: income.from,
        to: income.to,
        subject: income.subject,
        text: income.message,
        html: `<h1>Email Confirmation</h1>
            <div>
            <p>${income.message}</p>
            <a href=http://localhost:3000/${income.link}> Click here</a>
            </div>`,
      });
    } else if (verify) {
      return await transporter.sendMail({
        from: verify.from,
        to: verify.to,
        subject: verify.subject,
        text: verify.message,
        html: `<h1>Verification Status</h1>
            <div>
            <p>Hello ${verify.name}, Your have been verified. Please check your account!</p>
            <a href=http://localhost:3000/ > Click here to Login</a>
            </div>`,
      });
    }
  };

  correctPassword = async (candidatePassword, userPassword) => {
    return await bcrypt.compare(candidatePassword, userPassword);
  };

  accessToken = (payload, private_key, admin) => {
    let exp;
    if (admin) exp = "10000d";
    else exp = "365d";
    return jwt.sign(payload, private_key, {
      expiresIn: exp,
      algorithm: "RS256",
    });
  };
}

module.exports = utils;
