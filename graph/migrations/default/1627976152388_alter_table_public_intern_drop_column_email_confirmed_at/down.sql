alter table "public"."intern" alter column "email_confirmed_at" drop not null;
alter table "public"."intern" add column "email_confirmed_at" text;
