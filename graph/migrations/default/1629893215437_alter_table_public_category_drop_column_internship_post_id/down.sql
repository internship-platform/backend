alter table "public"."category"
  add constraint "catagory_internship_post_id_fkey"
  foreign key (internship_post_id)
  references "public"."internship_post"
  (id) on update cascade on delete cascade;
alter table "public"."category" alter column "internship_post_id" drop not null;
alter table "public"."category" add column "internship_post_id" int4;
