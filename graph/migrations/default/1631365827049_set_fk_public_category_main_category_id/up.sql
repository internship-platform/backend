alter table "public"."category"
  add constraint "category_main_category_id_fkey"
  foreign key ("main_category_id")
  references "public"."main_category"
  ("id") on update cascade on delete cascade;
