-- INSERT INTO internship_post (id, company_id, description, title) VALUES
--   (3, 2, 'Recipe on making the best fajitas in the world', 'Civil Engineer'),
--   (5, 1, 'Guide on successfully climbing the hightest mountain in the world', 'Data Encoder'),
--   (6, 3, 'What it takes for you to be a successful performer at broadway', 'Environmental engineering');

INSERT INTO category(id, name) VALUES
(1, 'Software Engineering'),
(2, 'Mechanical Engineering'), 
(3, 'Electrical Engineering'), 
(4, 'Civil Engineering'), 
(5, 'Electro-Mechanical'), 
(6, 'Medicine'), 
(7, 'Graphics');
