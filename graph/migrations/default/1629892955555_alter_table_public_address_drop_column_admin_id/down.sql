alter table "public"."address"
  add constraint "address_admin_id_fkey"
  foreign key (admin_id)
  references "public"."admin"
  (admin_id) on update cascade on delete cascade;
alter table "public"."address" alter column "admin_id" drop not null;
alter table "public"."address" add column "admin_id" text;
