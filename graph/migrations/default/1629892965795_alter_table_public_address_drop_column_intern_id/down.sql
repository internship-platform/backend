alter table "public"."address"
  add constraint "address_intern_id_fkey"
  foreign key (intern_id)
  references "public"."intern"
  (intern_id) on update cascade on delete cascade;
alter table "public"."address" alter column "intern_id" drop not null;
alter table "public"."address" add column "intern_id" text;
