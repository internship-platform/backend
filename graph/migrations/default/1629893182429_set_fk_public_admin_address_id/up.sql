alter table "public"."admin"
  add constraint "admin_address_id_fkey"
  foreign key ("address_id")
  references "public"."address"
  ("id") on update cascade on delete cascade;
