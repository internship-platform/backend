alter table "public"."application"
  add constraint "application_internship_post_id_fkey"
  foreign key ("internship_post_id")
  references "public"."internship_post"
  ("id") on update cascade on delete cascade;
