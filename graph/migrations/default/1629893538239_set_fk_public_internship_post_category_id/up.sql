alter table "public"."internship_post"
  add constraint "internship_post_category_id_fkey"
  foreign key ("category_id")
  references "public"."category"
  ("id") on update cascade on delete cascade;
