SET check_function_bodies = false;
CREATE TABLE public.address (
    id integer NOT NULL,
    region text NOT NULL,
    zone text NOT NULL,
    woreda text NOT NULL,
    city text NOT NULL,
    subcity text NOT NULL,
    kebele text NOT NULL,
    admin_id text,
    intern_id text,
    company_id text
);
CREATE SEQUENCE public.address_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.address_id_seq OWNED BY public.address.id;
CREATE TABLE public.admin (
    admin_id text NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    email text NOT NULL,
    avatar text NOT NULL
);
CREATE TABLE public.application (
    id integer NOT NULL,
    intern_id text NOT NULL,
    internship_post_id integer NOT NULL,
    status text NOT NULL,
    created_at text NOT NULL,
    approved_date text NOT NULL
);
CREATE TABLE public.company (
    company_id text NOT NULL,
    name text NOT NULL,
    avatar text NOT NULL,
    email text NOT NULL,
    phone_no text NOT NULL,
    area_of_expertice text NOT NULL,
    bio text NOT NULL,
    pobox integer NOT NULL,
    website_url text NOT NULL,
    verified boolean DEFAULT false NOT NULL
);
CREATE TABLE public.company_comment (
    id integer NOT NULL,
    company_id text NOT NULL,
    intern_id text NOT NULL,
    created_at text NOT NULL,
    message text NOT NULL
);
CREATE SEQUENCE public.company_comment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.company_comment_id_seq OWNED BY public.company_comment.id;
CREATE TABLE public.fake_internship_post_report (
    id integer NOT NULL,
    intern_id text NOT NULL,
    intership_post_id integer NOT NULL,
    issued_date text NOT NULL,
    message text NOT NULL
);
CREATE SEQUENCE public.fake_internship_post_report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.fake_internship_post_report_id_seq OWNED BY public.fake_internship_post_report.id;
CREATE TABLE public.favorite (
    id integer NOT NULL,
    intern_id text NOT NULL,
    internship_post_id integer NOT NULL
);
CREATE SEQUENCE public.favorite_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.favorite_id_seq OWNED BY public.favorite.id;
CREATE TABLE public.intern (
    intern_id text NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    email text NOT NULL,
    avatar text NOT NULL,
    bio text NOT NULL,
    cv_url text NOT NULL,
    phone_no text NOT NULL,
    name_of_studying_org text NOT NULL,
    birth_date text NOT NULL
);
CREATE TABLE public.internship_post (
    id integer NOT NULL,
    company_id text NOT NULL,
    description text NOT NULL,
    title text NOT NULL,
    category text NOT NULL,
    created_at text NOT NULL,
    expire_date text NOT NULL,
    active boolean DEFAULT true NOT NULL
);
CREATE TABLE public.internship_post_comment (
    id integer NOT NULL,
    intern_id text NOT NULL,
    internship_post_id integer NOT NULL,
    created_at text NOT NULL,
    message text NOT NULL
);
CREATE SEQUENCE public.internship_post_comment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.internship_post_comment_id_seq OWNED BY public.internship_post_comment.id;
CREATE SEQUENCE public.internship_post_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.internship_post_id_seq OWNED BY public.internship_post.id;
CREATE TABLE public.like_company (
    id integer NOT NULL,
    company_id text NOT NULL,
    intern_id text NOT NULL,
    liked boolean DEFAULT false NOT NULL
);
CREATE SEQUENCE public.like_company_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.like_company_id_seq OWNED BY public.like_company.id;
CREATE TABLE public.rating (
    id integer NOT NULL,
    intern_id text NOT NULL,
    internship_post_id integer NOT NULL,
    value double precision NOT NULL
);
CREATE SEQUENCE public.rating_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.rating_id_seq OWNED BY public.rating.id;
CREATE TABLE public.skill_set (
    id integer NOT NULL,
    intern_id text NOT NULL,
    name text NOT NULL,
    skill_value text NOT NULL,
    skill_catagory text NOT NULL
);
CREATE SEQUENCE public.skill_sets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.skill_sets_id_seq OWNED BY public.skill_set.id;
CREATE TABLE public.social_media_link (
    id integer NOT NULL,
    intern_id text NOT NULL,
    company_id text NOT NULL,
    twitter text NOT NULL,
    linkedin text NOT NULL,
    github_or_gitlab text NOT NULL
);
CREATE SEQUENCE public.social_media_links_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.social_media_links_id_seq OWNED BY public.social_media_link.id;
ALTER TABLE ONLY public.address ALTER COLUMN id SET DEFAULT nextval('public.address_id_seq'::regclass);
ALTER TABLE ONLY public.company_comment ALTER COLUMN id SET DEFAULT nextval('public.company_comment_id_seq'::regclass);
ALTER TABLE ONLY public.fake_internship_post_report ALTER COLUMN id SET DEFAULT nextval('public.fake_internship_post_report_id_seq'::regclass);
ALTER TABLE ONLY public.favorite ALTER COLUMN id SET DEFAULT nextval('public.favorite_id_seq'::regclass);
ALTER TABLE ONLY public.internship_post ALTER COLUMN id SET DEFAULT nextval('public.internship_post_id_seq'::regclass);
ALTER TABLE ONLY public.internship_post_comment ALTER COLUMN id SET DEFAULT nextval('public.internship_post_comment_id_seq'::regclass);
ALTER TABLE ONLY public.like_company ALTER COLUMN id SET DEFAULT nextval('public.like_company_id_seq'::regclass);
ALTER TABLE ONLY public.rating ALTER COLUMN id SET DEFAULT nextval('public.rating_id_seq'::regclass);
ALTER TABLE ONLY public.skill_set ALTER COLUMN id SET DEFAULT nextval('public.skill_sets_id_seq'::regclass);
ALTER TABLE ONLY public.social_media_link ALTER COLUMN id SET DEFAULT nextval('public.social_media_links_id_seq'::regclass);
ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.admin
    ADD CONSTRAINT admin_email_key UNIQUE (email);
ALTER TABLE ONLY public.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (admin_id);
ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.company_comment
    ADD CONSTRAINT company_comment_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_email_key UNIQUE (email);
ALTER TABLE ONLY public.company
    ADD CONSTRAINT company_pkey PRIMARY KEY (company_id);
ALTER TABLE ONLY public.fake_internship_post_report
    ADD CONSTRAINT fake_internship_post_report_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.favorite
    ADD CONSTRAINT favorite_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.intern
    ADD CONSTRAINT intern_email_key UNIQUE (email);
ALTER TABLE ONLY public.intern
    ADD CONSTRAINT intern_pkey PRIMARY KEY (intern_id);
ALTER TABLE ONLY public.internship_post_comment
    ADD CONSTRAINT internship_post_comment_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.internship_post
    ADD CONSTRAINT internship_post_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.like_company
    ADD CONSTRAINT like_company_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.rating
    ADD CONSTRAINT rating_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.skill_set
    ADD CONSTRAINT skill_sets_name_key UNIQUE (name);
ALTER TABLE ONLY public.skill_set
    ADD CONSTRAINT skill_sets_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.social_media_link
    ADD CONSTRAINT social_media_links_company_id_key UNIQUE (company_id);
ALTER TABLE ONLY public.social_media_link
    ADD CONSTRAINT social_media_links_intern_id_key UNIQUE (intern_id);
ALTER TABLE ONLY public.social_media_link
    ADD CONSTRAINT social_media_links_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_admin_id_fkey FOREIGN KEY (admin_id) REFERENCES public.admin(admin_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.company(company_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_intern_id_fkey FOREIGN KEY (intern_id) REFERENCES public.intern(intern_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_intern_id_fkey FOREIGN KEY (intern_id) REFERENCES public.intern(intern_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.application
    ADD CONSTRAINT application_internship_post_id_fkey FOREIGN KEY (internship_post_id) REFERENCES public.internship_post(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.company_comment
    ADD CONSTRAINT company_comment_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.company(company_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.company_comment
    ADD CONSTRAINT company_comment_intern_id_fkey FOREIGN KEY (intern_id) REFERENCES public.intern(intern_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.fake_internship_post_report
    ADD CONSTRAINT fake_internship_post_report_intern_id_fkey FOREIGN KEY (intern_id) REFERENCES public.intern(intern_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.fake_internship_post_report
    ADD CONSTRAINT fake_internship_post_report_intership_post_id_fkey FOREIGN KEY (intership_post_id) REFERENCES public.internship_post(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.favorite
    ADD CONSTRAINT favorite_intern_id_fkey FOREIGN KEY (intern_id) REFERENCES public.intern(intern_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.favorite
    ADD CONSTRAINT favorite_internship_post_id_fkey FOREIGN KEY (internship_post_id) REFERENCES public.internship_post(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.internship_post_comment
    ADD CONSTRAINT internship_post_comment_intern_id_fkey FOREIGN KEY (intern_id) REFERENCES public.intern(intern_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.internship_post_comment
    ADD CONSTRAINT internship_post_comment_internship_post_id_fkey FOREIGN KEY (internship_post_id) REFERENCES public.internship_post(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.internship_post
    ADD CONSTRAINT internship_post_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.company(company_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.like_company
    ADD CONSTRAINT like_company_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.company(company_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.like_company
    ADD CONSTRAINT like_company_intern_id_fkey FOREIGN KEY (intern_id) REFERENCES public.intern(intern_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.rating
    ADD CONSTRAINT rating_intern_id_fkey FOREIGN KEY (intern_id) REFERENCES public.intern(intern_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.rating
    ADD CONSTRAINT rating_internship_post_id_fkey FOREIGN KEY (internship_post_id) REFERENCES public.internship_post(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.skill_set
    ADD CONSTRAINT skill_set_intern_id_fkey FOREIGN KEY (intern_id) REFERENCES public.intern(intern_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.social_media_link
    ADD CONSTRAINT social_media_link_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.company(company_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.social_media_link
    ADD CONSTRAINT social_media_link_intern_id_fkey FOREIGN KEY (intern_id) REFERENCES public.intern(intern_id) ON UPDATE CASCADE ON DELETE CASCADE;
