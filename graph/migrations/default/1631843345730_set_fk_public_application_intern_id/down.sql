alter table "public"."application" drop constraint "application_intern_id_fkey",
  add constraint "application_intern_id_fkey"
  foreign key ("intern_id")
  references "public"."intern"
  ("intern_id") on update cascade on delete cascade;
