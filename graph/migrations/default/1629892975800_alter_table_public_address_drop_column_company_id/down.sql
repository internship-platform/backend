alter table "public"."address"
  add constraint "address_company_id_fkey"
  foreign key (company_id)
  references "public"."company"
  (company_id) on update cascade on delete cascade;
alter table "public"."address" alter column "company_id" drop not null;
alter table "public"."address" add column "company_id" text;
