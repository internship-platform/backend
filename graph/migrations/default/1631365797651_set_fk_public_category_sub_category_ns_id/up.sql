alter table "public"."category"
  add constraint "category_sub_category_ns_id_fkey"
  foreign key ("sub_category_ns_id")
  references "public"."sub_category_ns"
  ("id") on update cascade on delete cascade;
