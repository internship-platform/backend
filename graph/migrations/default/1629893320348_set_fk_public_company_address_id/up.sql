alter table "public"."company"
  add constraint "company_address_id_fkey"
  foreign key ("address_id")
  references "public"."address"
  ("id") on update cascade on delete cascade;
