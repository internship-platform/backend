alter table "public"."like_company" alter column "liked" set default false;
alter table "public"."like_company" alter column "liked" drop not null;
alter table "public"."like_company" add column "liked" bool;
