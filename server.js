const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const actionMiddleware = require("./middleware/actionMiddleware");

const app = express();

app.use(morgan("combined"));
app.use(cors());
app.use(actionMiddleware);

const port = process.env.EXPRESS_PORT;

app.use(express.json({ limit: "100mb" }));

app.post("/confirm_email", require("./handlers/confirm_email"));
app.post("/company_signup", require("./handlers/company/signup"));
app.post("/intern_signup", require("./handlers/intern/intern_signup"));
app.post("/change_email_confirm", require("./handlers/change_email_confirm"));
app.post("/intern_login", require("./handlers/intern/login"));
app.post("/company_login", require("./handlers/company/login"));
app.post("/upload_files", require("./handlers/upload"));
app.post("/reset", require("./handlers/reset"));
app.post("/forgot_password", require("./handlers/forgot_password"));
app.post("/update_password", require("./handlers/update_password"));
app.post("/admin_login", require("./handlers/admin/login"));
app.post("/admin_register", require("./handlers/admin/register"));
app.post("/create_users", require("./handlers/admin/create"));
app.post("/change_email", require("./handlers/change_email"));
app.post(
  "/application_status_email",
  require("./handlers/intern/application_status_email")
);
app.post(
  "/verification_status",
  require("./handlers/company/verificationStatus")
);

app.listen(port, () => {
  console.log(`Express server listening on ${port}`);
});
