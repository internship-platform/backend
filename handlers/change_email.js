const utils = require("./../utils/utils");

const util = new utils();

const change_email = async (req, res) => {
  const session_variables = req.body.session_variables;

  let email = req.body.input.email;
  let name = req.body.input.name;
  let role = session_variables["x-hasura-role"];
  let payload = {};

  try {
    if (role === "company") {
      payload = {
        company_id: session_variables["x-hasura-company-id"],
      };
    } else if (role === "intern") {
      payload = {
        intern_id: session_variables["x-hasura-intern-id"],
      };
    }

    const token = util.requireJWT(
      payload,
      process.env.CHANGE_EMAIL_CONFIRMATION_TOKEN
    );

    const income = {
      from: "no-reply@habelinternship.com",
      to: `${email}`,
      subject: `Hey, Welcome to Habel Internship!`,
      message: `Hello ${name} Please click the below link to verify your Email!`,
      link: `registration/confirm_email?change_email_confirm_token=${token}`,
    };
    await util.sendEmail(income);

    return res.json({
      accepted: true,
    });
  } catch (err) {
    return res.json({
      error: err.message,
      accepted: false,
    });
  }
};

module.exports = change_email;
