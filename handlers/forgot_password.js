const apollo_client = require("./apollo");
const utils = require("../utils/utils");
const jwt = require("jsonwebtoken");
const gql = require("graphql-tag");

const util = new utils();

const forgot_password = async (req, res) => {
  try {
    let decoded = jwt.verify(
      req.body.input.change_password_confirmation_token,
      process.env.CHANGE_PASSWORD_CONFIRMATION_TOKEN
    );

    if (!decoded.company_id && !decoded.admin_id && !decoded.intern_id) {
      throw new Error("Change password confirmation token is malformed");
    }

    let dateNow = new Date();

    let tokenTime = decoded.iat * 1000;

    let hours = 1;
    let tokenLife = hours * 60 * 1000;

    if (tokenTime + tokenLife > dateNow.getTime()) {
      throw new Error("Change password confirmation token is expired");
    }

    let { password } = await util.passwordIdDate(req.body.input.password);

    let newPassword = password;

    if (decoded.company_id) {
      let update = gql`
        mutation ($company_id: String!, $password: String!) {
          update_company_by_pk(
            pk_columns: { company_id: $company_id }
            _set: { password: $password }
          ) {
            company_id
          }
        }
      `;

      let { data } = await apollo_client.mutate({
        mutation: update,
        variables: {
          company_id: decoded.company_id,
          password: newPassword,
        },
      });

      return res.json({
        company_id: data.update_company_by_pk.company_id,
      });
    } else if (decoded.intern_id) {
      let update = gql`
        mutation ($intern_id: String!, $password: String!) {
          update_intern_by_pk(
            pk_columns: { intern_id: $intern_id }
            _set: { password: $password }
          ) {
            intern_id
          }
        }
      `;
      let { data } = await apollo_client.mutate({
        mutation: update,
        variables: {
          intern_id: decoded.intern_id,
          password: newPassword,
        },
      });

      return res.json({
        intern_id: data.update_intern_by_pk.intern_id,
      });
    } else if (decoded.admin_id) {
      let update = gql`
        mutation ($admin_id: String!, $password: String!) {
          update_admin_by_pk(
            pk_columns: { admin_id: $admin_id }
            _set: { password: $password }
          ) {
            admin_id
          }
        }
      `;
      let { data } = await apollo_client.mutate({
        mutation: update,
        variables: {
          admin_id: decoded.admin_id,
          password: newPassword,
        },
      });

      return res.json({
        admin_id: data.update_admin_by_pk.admin_id,
      });
    }
  } catch (err) {
    console.log(err);
    return res.json({
      error: err.message,
    });
  }
};

module.exports = forgot_password;
