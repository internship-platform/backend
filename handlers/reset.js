const utils = require("./../utils/utils");

const util = new utils();

const reset = async (req, res) => {
  let email = req.body.input.email;
  let name = req.body.input.name;
  let role = req.body.input.role;
  let id = req.body.input.id;
  let payload = {};
  console.log("Input -", req.body.input);
  try {
    if (role === "company") {
      payload = {
        company_id: id,
      };
    } else if (role === "intern") {
      payload = {
        intern_id: id,
      };
    } else if (role === "myadmin") {
      payload = {
        admin_id: id,
      };
    }

    const token = util.requireJWT(
      payload,
      process.env.CHANGE_PASSWORD_CONFIRMATION_TOKEN
    );

    const income = {
      from: "no-reply@habelinternship.com",
      to: `${email}`,
      subject: `Hey, Welcome to Habel Internship!`,
      message: `Hello ${name} Please click the below link to change your password!`,
      link: `forget/change_password?change_password_token=${token}`,
    };

    await util.sendEmail(income);

    return res.json({
      accepted: true,
    });
  } catch (err) {
    return res.json({
      error: err.message,
      accepted: false,
    });
  }
};

module.exports = reset;
