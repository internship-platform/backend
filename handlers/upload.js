const cloudinary = require("cloudinary").v2;

module.exports = async (req, res) => {
  try {
    const { files, folder } = req.body.input;
    console.log("File - ",files)
    let secure_urls = [];

    for (let file in files) {
      file = files[file];

      let data = await cloudinary.uploader.upload(file, {
        unique_filename: true,
        discard_original_filename: true,
        folder: folder,
        timeout: 120000,
      });

      secure_urls.push(data.secure_url);

      console.log("Secure Url", secure_urls);
    }

    res.json({
      secure_urls,
    });
  } catch (error) {
    console.error(error);

    res.status(500).send({
      error: "Error Uploading Files",
    });
  }
};
