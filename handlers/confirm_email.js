const jwt = require("jsonwebtoken");
const gql = require("graphql-tag");
const apollo_client = require("./apollo");

const confirm_email = async (req, res) => {
  try {
    let decoded = jwt.verify(
      req.body.input.email_confirmation_token,
      process.env.EMAIL_CONFIRMATION_TOKEN
    );

    if (!decoded.company_id && !decoded.intern_id) {
      throw Error("Email confirmation token is malformed");
    }

    let dateNow = new Date();
    let tokenTime = decoded.iat * 1000;

    let hours = 1;
    let tokenLife = hours * 60 * 1000;

    if (tokenTime + tokenLife > dateNow.getTime()) {
      throw Error("Email confirmation token is expired");
    }

    let payload;

    if (decoded.intern_id) {
      let intern_id = decoded.intern_id;

      let result = await apollo_client.mutate({
        mutation: gql`
          mutation ($intern_id: String!, $email_confirmed_at: date!) {
            update_intern_by_pk(
              pk_columns: { intern_id: $intern_id }
              _set: { active: true, email_confirmed_at: $email_confirmed_at }
            ) {
              active
              intern_id
              first_name
              last_name
              phone_no
              email
              avatar
            }
          }
        `,
        variables: {
          intern_id,
          email_confirmed_at: new Date(),
        },
      });

      return res.json({
        intern_id,
      });
    } else if (decoded.company_id) {
      let company_id = decoded.company_id;

      let resp = await apollo_client.mutate({
        mutation: gql`
          mutation ($company_id: String!, $email_confirmed_at: date!) {
            update_company_by_pk(
              pk_columns: { company_id: $company_id }
              _set: { active: true, email_confirmed_at: $email_confirmed_at }
            ) {
              active
              company_id
              avatar
              email
              name
              phone_no
            }
          }
        `,
        variables: {
          company_id,
          email_confirmed_at: new Date(),
        },
      });
      return res.json({
        company_id,
        name: resp.data.update_company_by_pk.name,
      });
    }
  } catch (err) {
    return res.json({
      error: err.message,
    });
  }
};

module.exports = confirm_email;
