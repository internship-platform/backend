const apollo_client = require("./apollo");
const utils = require("./../utils/utils");
const gql = require("graphql-tag");

const util = new utils();

const update_password = async (req, res) => {
  const session_variables = req.body.session_variables;
  let role = session_variables["x-hasura-role"];

  try {
    if (role === "intern") {
      let intern_id = req.body.input.id;
      let q = gql`
        query ($intern_id: String!) {
          intern(where: { intern_id: { _eq: $intern_id } }) {
            intern_id
            password
          }
        }
      `;

      let { data } = await apollo_client.query({
        query: q,
        variables: {
          intern_id,
        },
      });

      if (!data.intern[0]) throw new Error("User is not found");

      let val = await util.correctPassword(
        req.body.input.oldPassword,
        data.intern[0].password
      );

      if (!val) {
        throw new Error("Incorrect Old Password");
      }

      let { password } = await util.passwordIdDate(req.body.input.newPassword);

      let update = gql`
        mutation ($intern_id: String!, $password: String!) {
          update_intern_by_pk(
            pk_columns: { intern_id: $intern_id }
            _set: { password: $password }
          ) {
            intern_id
          }
        }
      `;
      let { data: result } = await apollo_client.mutate({
        mutation: update,
        variables: {
          intern_id,
          password,
        },
      });

      return res.json({
        intern_id,
      });
    } else if (role === "company") {
      let company_id = req.body.input.id;
      let q = gql`
        query ($company_id: String!) {
          company(where: { company_id: { _eq: $company_id } }) {
            company_id
            password
          }
        }
      `;

      let { data } = await apollo_client.query({
        query: q,
        variables: {
          company_id,
        },
      });

      if (!data.company[0]) throw new Error("User is not found");

      let val = await util.correctPassword(
        req.body.input.oldPassword,
        data.company[0].password
      );

      if (!val) {
        throw new Error("Incorrect Old Password");
      }

      let { password } = await util.passwordIdDate(req.body.input.newPassword);

      let update = gql`
        mutation ($company_id: String!, $password: String!) {
          update_company_by_pk(
            pk_columns: { company_id: $company_id }
            _set: { password: $password }
          ) {
            company_id
          }
        }
      `;
      let { data: result } = await apollo_client.mutate({
        mutation: update,
        variables: {
          company_id,
          password,
        },
      });

      return res.json({
        company_id,
      });
    } else if (role === "myadmin") {
      let admin_id = req.body.input.id;
      let q = gql`
        query ($admin_id: String!) {
          admin(where: { admin_id: { _eq: $admin_id } }) {
            admin_id
            password
          }
        }
      `;

      let { data } = await apollo_client.query({
        query: q,
        variables: {
          admin_id,
        },
      });

      if (!data.admin[0]) throw new Error("User is not found");

      let val = await util.correctPassword(
        req.body.input.oldPassword,
        data.admin[0].password
      );

      if (!val) {
        throw new Error("Incorrect Old Password");
      }

      let { password } = await util.passwordIdDate(req.body.input.newPassword);

      let update = gql`
        mutation ($admin_id: String!, $password: String!) {
          update_admin_by_pk(
            pk_columns: { admin_id: $admin_id }
            _set: { password: $password }
          ) {
            admin_id
          }
        }
      `;
      let { data: result } = await apollo_client.mutate({
        mutation: update,
        variables: {
          admin_id,
          password,
        },
      });

      return res.json({
        admin_id,
      });
    }
  } catch (err) {
    console.error(err);
    return res.json({
      error: err.message,
    });
  }
};

module.exports = update_password;
