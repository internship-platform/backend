const utils = require("../../utils/utils");
const gql = require("graphql-tag");
const apollo_client = require("../apollo");

const util = new utils();

const verification_status = async (req, res) => {
  try {
    let email = req.body.input.email;
    let name = req.body.input.name;

    const verify = {
      from: "no-reply@habelinternship.com",
      to: `${email}`,
      subject: `Hey, Welcome to Habel Internship!`,
      name: name,
    };

    await util.sendEmail(...[, ,], verify);

    return res.json({
      send: true,
    });
  } catch (err) {
    console.log(err);
    return res.json({
      error: err.message,
      send: false,
    });
  }
};

module.exports = verification_status;
