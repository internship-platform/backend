const utils = require("./../../utils/utils");
const apollo_client = require("../apollo");
const gql = require("graphql-tag");
const fs = require("fs");

const util = new utils();

let private_key;

try {
  private_key = fs.readFileSync(process.env.PRIVATE_KEY, "utf8");
} catch (error) {
  console.error(error);
  process.exit(1);
}

const companyLogin = async (req, res) => {
  try {
    const email = req.body.input.input.email;

    let q = gql`
      query ($email: String!) {
        company(where: { email: { _eq: $email } }) {
          company_id
          name
          email
          phone_no
          website_url
          verified
          password
          active
        }
      }
    `;

    let { data } = await apollo_client.query({
      query: q,
      variables: {
        email,
      },
    });

    if (!data.company[0]) throw new Error("User is not found");

    if (!data.company[0].active)
      throw new Error("Email is not Confirmed. Please Confirm your email");

    let val = await util.correctPassword(
      req.body.input.input.password,
      data.company[0].password
    );

    if (!val) {
      throw new Error("Incorrect Email or Password");
    }

    payload = {
      sub: data.company[0].company_id,
      aud: "Habel Internship Platform",
      iat: Math.floor(Date.now() / 1000),
      "https://hasura.io/jwt/claims": {
        "x-hasura-allowed-roles": ["company"],
        "x-hasura-default-role": "company",
        "x-hasura-company-id": data.company[0].company_id,
        phone_no: data.company[0].phone_no,
        name: data.company[0].name,
        email: data.company[0].email,
        avatar: data.company[0].avatar,
        verified: data.company[0].verified,
        website_url: data.company[0].website_url,
      },
    };

    const token = util.accessToken(payload, private_key);

    return res.json({
      accessToken: token,
    });
  } catch (error) {
    return res.json({
      error: error.message,
    });
  }
};

module.exports = companyLogin;
