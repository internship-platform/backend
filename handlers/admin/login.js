const utils = require("./../../utils/utils");
const apollo_client = require("../apollo");
const gql = require("graphql-tag");
const fs = require("fs");

const util = new utils();

let private_key;

try {
  private_key = fs.readFileSync(process.env.PRIVATE_KEY, "utf8");
} catch (error) {
  console.error(error);
  process.exit(1);
}

const adminLogin = async (req, res) => {
  try {
    const email = req.body.input.email;

    let q = gql`
      query ($email: String!) {
        admin(where: { email: { _eq: $email } }) {
          avatar
          email
          first_name
          last_name
          admin_id
          password
        }
      }
    `;

    let { data } = await apollo_client.query({
      query: q,
      variables: {
        email,
      },
    });

    if (!data.admin[0]) throw new Error("User is not found");

    let val = await util.correctPassword(
      req.body.input.password,
      data.admin[0].password
    );

    if (!val) {
      throw new Error("Incorrect Email or Password");
    }

    payload = {
      sub: data.admin[0].admin_id,
      aud: "Habel Internship Platform",
      iat: Math.floor(Date.now() / 1000),
      first_name: data.admin[0].first_name,
      last_name: data.admin[0].last_name,
      "https://hasura.io/jwt/claims": {
        "x-hasura-allowed-roles": ["myadmin"],
        "x-hasura-default-role": "myadmin",
        "x-hasura-admin-id": data.admin[0].admin_id,
        name: `${data.admin[0].first_name} ${data.admin[0].last_name}`,
        email: data.admin[0].email,
        avatar: data.admin[0].avatar,
      },
    };

    const token = util.accessToken(payload, private_key, "admin");

    return res.json({
      accessToken: token,
    });
  } catch (error) {
    console.error(error);
    return res.json({
      error: error.message,
    });
  }
};

module.exports = adminLogin;
