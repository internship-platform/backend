const utils = require("./../../utils/utils");
const apollo_client = require("../apollo");
const gql = require("graphql-tag");

const util = new utils();

const createUsers = async (req, res) => {
  try {
    const { input } = req.body.input;
    let { password, id } = await util.passwordIdDate(input.password);

    input.password = password;
    input.company_id = id;
    delete input.confirm_password;

    let { data: result } = await apollo_client.mutate({
      mutation: gql`
        mutation (
          $city: String!
          $kebele: String
          $region: String!
          $subcity: String
          $woreda: String!
          $zone: String
        ) {
          insert_address(
            objects: {
              city: $city
              kebele: $kebele
              region: $region
              subcity: $subcity
              woreda: $woreda
              zone: $zone
            }
          ) {
            returning {
              id
              kebele
              zone
              subcity
              woreda
              city
              region
            }
          }
        }
      `,
      variables: {
        city: input.city,
        kebele: input.kebele,
        region: input.region,
        subcity: input.subcity,
        woreda: input.woreda,
        zone: input.zone,
      },
    });

    let { data } = await apollo_client.mutate({
      mutation: gql`
        mutation (
          $area_of_expertise: String!
          $company_id: String!
          $name: String!
          $email: String!
          $phone_no: String!
          $pobox: Int
          $website_url: String
          $password: String!
          $address_id: Int
        ) {
          insert_company(
            objects: {
              area_of_expertise: $area_of_expertise
              company_id: $company_id
              email: $email
              name: $name
              phone_no: $phone_no
              pobox: $pobox
              website_url: $website_url
              password: $password
              address_id: $address_id
            }
          ) {
            returning {
              company_id
              email
              website_url
              phone_no
              name
              pobox
            }
          }
        }
      `,
      variables: {
        area_of_expertise: input.area_of_expertise,
        company_id: input.company_id,
        email: input.email,
        name: input.name,
        phone_no: input.phone_no,
        pobox: input.pobox,
        website_url: input.website_url,
        password: input.password,
        address_id: result.insert_address.returning[0].id,
      },
    });

    const payload = {
      company_id: input.company_id,
    };

    const token = util.requireJWT(
      payload,
      process.env.EMAIL_CONFIRMATION_TOKEN
    );

    const income = {
      from: "no-reply@habelinternship.com",
      to: `${data.insert_company.returning[0].email}`,
      subject: `Hey, Welcome to Habel Internship!`,
      message: `Hello ${data.insert_company.returning[0].name} Please click the below link to verify the email!`,
      link: `registration/confirm_email?confirm_email_token=${token}`,
    };
    await util.sendEmail(income);

    return res.status(200).json({
      message: "Successfully signed up",
    });
  } catch (error) {
    console.error(error);
    return res.json({
      error: error.message,
    });
  }
};

module.exports = createUsers;
