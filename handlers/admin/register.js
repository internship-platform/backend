const utils = require("../../utils/utils");
const gql = require("graphql-tag");
const apollo_client = require("../apollo");

const util = new utils();

const register = async (req, res) => {
  let { input } = req.body.input;

  try {
    let { password, id } = await util.passwordIdDate(input.password);

    input.password = password;
    input.admin_id = id;

    let { data } = await apollo_client.mutate({
      mutation: gql`
        mutation (
          $last_name: String!
          $admin_id: String!
          $first_name: String!
          $email: String!
          $password: String!
        ) {
          insert_admin_one(
            object: {
              admin_id: $admin_id
              email: $email
              first_name: $first_name
              last_name: $last_name
              password: $password
            }
          ) {
            admin_id
            email
            first_name
            last_name
          }
        }
      `,
      variables: {
        ...input,
      },
    });

    return res.status(200).json({
      message: "Successfully signed up",
    });
  } catch (err) {
    return res.json({
      error: err.message,
    });
  }
};

module.exports = register;
