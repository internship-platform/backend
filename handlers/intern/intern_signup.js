const utils = require("../../utils/utils");
const gql = require("graphql-tag");
const apollo_client = require("./../apollo");

const util = new utils();

const internSignup = async (req, res) => {
  try {
    let { input } = req.body.input;

    if (input.password !== input.confirm_password) {
      throw new Error("Password and Confirm Password mismatch");
    } else {
      let { password, id, confirm_password } = await util.passwordIdDate(
        input.password
      );

      delete input.confirm_password;
      delete input.password;

      let { data } = await apollo_client.mutate({
        mutation: gql`
          mutation (
            $intern_id: String!
            $first_name: String!
            $last_name: String!
            $email: String!
            $phone_no: String!
            $gender: String!
            $password: String!
          ) {
            insert_intern_one(
              object: {
                email: $email
                password: $password
                phone_no: $phone_no
                gender: $gender
                first_name: $first_name
                last_name: $last_name
                intern_id: $intern_id
              }
            ) {
              intern_id
              email
              password
              phone_no
              first_name
              last_name
              gender
            }
          }
        `,
        variables: {
          intern_id: id,
          ...input,
          password,
        },
      });

      const payload = {
        intern_id: data.insert_intern_one.intern_id,
      };

      const token = util.requireJWT(
        payload,
        process.env.EMAIL_CONFIRMATION_TOKEN
      );

      const income = {
        from: "no-reply@habelinternship.com",
        to: `${data.insert_intern_one.email}`,
        subject: `Hey ${
          data.insert_intern_one.gender === "M" ? "Sir" : "Madam"
        }, Welcome to Habel Internship!`,
        message: `Hello ${data.insert_intern_one.first_name} Please click the below link to verify the email!`,
        link: `registration/confirm_email?confirm_email_token=${token}`,
      };
      await util.sendEmail(income);

      return res.status(200).json({
        message: "Successfully signed up",
      });
    }
  } catch (err) {
    return res.json({
      error: err.message,
    });
  }
};

module.exports = internSignup;
