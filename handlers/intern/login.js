const utils = require("./../../utils/utils");
const apollo_client = require("../apollo");
const gql = require("graphql-tag");
const fs = require("fs");

const util = new utils();

let private_key;

try {
  private_key = fs.readFileSync(process.env.PRIVATE_KEY, "utf8");
} catch (error) {
  console.error(error);
  process.exit(1);
}

const internLogin = async (req, res) => {
  try {
    const email = req.body.input.input.email;

    let q = gql`
      query ($email: String!) {
        intern(where: { email: { _eq: $email } }) {
          intern_id
          first_name
          last_name
          email
          password
          phone_no
          avatar
          active
        }
      }
    `;

    let { data } = await apollo_client.query({
      query: q,
      variables: {
        email,
      },
    });

    if (!data.intern[0]) throw new Error("User is not found");

    if (!data.intern[0].active)
      throw new Error("Email is not Confirmed. Please Confirm your email");

    let val = await util.correctPassword(
      req.body.input.input.password,
      data.intern[0].password
    );

    if (!val) {
      throw new Error("Incorrect Email or Password");
    }

    payload = {
      sub: data.intern[0].intern_id,
      aud: "Habel Internship Platform",
      iat: Math.floor(Date.now() / 1000),
      "https://hasura.io/jwt/claims": {
        "x-hasura-allowed-roles": ["intern"],
        "x-hasura-intern-id": data.intern[0].intern_id,
        phone_no: data.intern[0].phone_no,
        name: `${data.intern[0].first_name} ${data.intern[0].last_name}`,
        email: data.intern[0].email,
        avatar: data.intern[0].avatar,
        "x-hasura-role": "intern",
        "x-hasura-default-role": "intern",
      },
    };

    const token = util.accessToken(payload, private_key);

    return res.json({
      accessToken: token,
      intern_id: data.intern[0].intern_id,
    });
  } catch (error) {
    return res.json({
      error: error.message,
    });
  }
};

module.exports = internLogin;
