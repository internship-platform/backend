const utils = require("../../utils/utils");
const gql = require("graphql-tag");
const apollo_client = require("./../apollo");

const util = new utils();

const application_status_email = async (req, res) => {
  try {
    let email = req.body.input.email;
    let name = req.body.input.name;
    let postName = req.body.input.internshipPostName;

    const status = {
      from: "no-reply@habelinternship.com",
      to: `${email}`,
      subject: `Hey, Welcome to Habel Internship!`,
      message: `Hello ${name} Please check your account for more detail!`,
      post: postName,
    };

    await util.sendEmail(...[,], status);

    return res.json({
      send: true,
    });
  } catch (err) {
    return res.json({
      error: err.message,
      send: false,
    });
  }
};

module.exports = application_status_email;
