const jwt = require("jsonwebtoken");
const gql = require("graphql-tag");
const apollo_client = require("./apollo");

const change_email_confirm = async (req, res) => {
  try {
    let decoded = jwt.verify(
      req.body.input.change_email_confirmation_token,
      process.env.CHANGE_EMAIL_CONFIRMATION_TOKEN
    );

    if (!decoded.company_id && !decoded.intern_id) {
      throw new Error("Change Email confirmation token is malformed");
    }

    let dateNow = new Date();
    let tokenTime = decoded.iat * 1000;

    let hours = 1;
    let tokenLife = hours * 60 * 1000;

    if (tokenTime + tokenLife > dateNow.getTime()) {
      throw new Error("Change Email confirmation token is expired");
    }

    if (decoded.intern_id) {
      let { intern_id, email } = decoded;

      let result = await apollo_client.mutate({
        mutation: gql`
          mutation ($intern_id: String!, $email: String!) {
            update_intern_by_pk(
              pk_columns: { intern_id: $intern_id }
              _set: { email: $email }
            ) {
              intern_id
              email
            }
          }
        `,
        variables: {
          intern_id,
          email,
        },
      });

      return res.json({
        message: "Email successfully changed",
        intern_id,
        email: result.data.update_intern_by_pk.email,
      });
    } else if (decoded.company_id) {
      let { company_id, email } = decoded;

      let result = await apollo_client.mutate({
        mutation: gql`
          mutation ($company_id: String!, $email: String!) {
            update_company_by_pk(
              pk_columns: { company_id: $company_id }
              _set: { email: $email }
            ) {
              company_id
              email
              name
            }
          }
        `,
        variables: {
          company_id,
          email,
        },
      });

      return res.json({
        message: "Email successfully changed",
        company_id,
        email: result.data.update_company_by_pk.email,
        name: result.data.update_company_by_pk.name,
      });
    }
  } catch (err) {
    return res.json({
      error: err.message,
    });
  }
};

module.exports = change_email_confirm;
