FROM node:lts-alpine3.9

WORKDIR /usr/src/app

ARG EXPRESS_PORT

EXPOSE ${EXPRESS_PORT}

RUN apk add npm

COPY package.json package-lock.json ./

RUN npm install

COPY server.js .

COPY handlers ./handlers/

COPY utils ./utils/

COPY middleware ./middleware/

COPY private.pem /

COPY public.pem /

CMD ["node", "server.js"]
